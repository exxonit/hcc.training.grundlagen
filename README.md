# hcc.training 
Neue, leere Anwendung

###############################
Aufgabe 1: Bootstrap

Passen Sie den Pfad zum SAPUI5-Bootstrap-File so an, 
dass eine aktuell unterstützte Version der SAPUI5-Bibliotheken 
aus dem CDN der HANA Cloud Platform (HCP) geladen wird. Ändern
Sie außerdem das Theme auf sap_bluecrystal (bisher Standard).

###############################
Aufgabe 2: Views und Namespaces, JavaScript

a) Legen Sie einen weiteren View an (View_02). Stellen Sie darin
ein Control aus einer SAPUI5-Bibliothek dar, die noch nicht
verwendet wurde (die Explored App liefert Beispielcoding).
Ändern Sie im manifest.json den rootView auf View_02.

b) Implementieren Sie in dem neuen Control ein press-Event und
überprüfen Sie, dass die zugehörige Funktion im Controller
durchlaufen wird mit dem Debugger der Developer Tools im Browser 
(z.B. Chrome). 

c) Verfolgen Sie das Verhalten einer for-Schleife (in der eben
erstellten Funktion). Überprüfen Sie das korrekte Verhalten
einer if-Bedingung. 

###############################
Aufgabe 3: Navigation, Wert auslesen

a) Implementieren Sie in beiden Views (Main und View_02) jeweils
einen Button, der sie beim Klicken zum anderen View navigiert.

b) Ändern Sie den bisher statischen Text in Main.view in ein
sap.m.Input. Erweitern Sie den View zusätzlich um einen weiteren 
Button, der beim Klick den Wert aus dem Input in einem 
MessageToast ausgibt.

###############################
Aufgabe 4: Internationalisierung

Legen Sie eine deutsche i18n-Datei an und pflegen Sie darin
und in der i18n.properties mindestens 2 Texte, die Sie in ihrer
SAPUI5-Anwendung darstellen. Überprüfen Sie die korrekte 
Verwendung der Texte mit Hilfe der Run Configuration
(Web Application with Frame).